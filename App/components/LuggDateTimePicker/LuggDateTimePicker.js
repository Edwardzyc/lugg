

import * as React from "react";
import {
  View,
  Dimensions,
} from "react-native";
import Carousel from 'react-native-snap-carousel';
import { thirtyDays } from '../../utilities/date'
import { LuggDateItem } from '../LuggDateItem/LuggDateItem'
import { LuggTimeItem } from '../LuggTimeItem/LuggTimeItem'

export class LuggDateTimePicker extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      dates: thirtyDays(),
      currentDateIndex: 0
    }
  }

  _renderItem = ({ item: date, index }) => {
    return (
      <LuggDateItem 
        itemIndex={index} 
        currentItem={this.refs.datecarousel.currentIndex === index} 
        date={date}
      />
    );
  }

  __renderItem = ({ item, index }) => {
    return (
      <LuggTimeItem time={item} currentItem={this.refs.tcarousel.currentIndex === index} />
    )
  }

  timeData = () => {
    const { currentDateIndex } = this.state
    const data = ['5:00 PM - 6:00 PM', '6:00 PM - 7:00 PM', '7:00 PM - 8:00 PM', '9:00 PM - 10:00 PM']
    if (currentDateIndex === 0) {
      return [ 'Within the hour' , ...data]
    } else {
      return data
    }
  }
  render() {
    return (
      <React.Fragment>
        <View style={{
          height: 60,
        }}>
          <Carousel
            onSnapToItem={(slideIndex) => {
              this.setState({
                currentDateIndex: slideIndex
              })
            }}	
            ref={'datecarousel'}
            data={this.state.dates}
            renderItem={this._renderItem}
            inactiveSlideScale={1.0}
            inactiveSlideOpacity={1.0}
            sliderWidth={Dimensions.get('screen').width}
            sliderHeight={60}
            itemWidth={Dimensions.get('screen').width / 3}
            itemHeight={60}
            contentContainerCustomStyle={{
              backgroundColor: '#313970',
            }}

          />
        </View>
        <View>
          <Carousel
            containerCustomStyle={{
              backgroundColor: '#293065',
            }}
            onSnapToItem={() => {
              this.forceUpdate()
            }}	
            vertical={true}
            ref={'tcarousel'}
            inactiveSlideScale={1.0}
            inactiveSlideOpacity={1.0}
            data={this.timeData()}
            renderItem={this.__renderItem}
            itemHeight={60}
            sliderHeight={180}
          />
        </View>
      </React.Fragment>
    )
  }
}


