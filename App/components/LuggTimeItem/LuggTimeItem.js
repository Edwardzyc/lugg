import * as React from "react";
import {
  View,
  StyleSheet,
  Text
} from "react-native";

export function LuggTimeItem(props) {
  return (
    <View style={[s.item, props.currentItem ? s.activeItem : null]}>
        <Text style={[s.itemText, props.currentItem ? s.activeItemText : null]}>
          { props.time }
        </Text>
      </View>
  )
}

const s = StyleSheet.create({
  itemText: {
    color: 'white',
  },
  activeItemText: {
    color: '#4e547e',
  },
  item: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    backgroundColor: '#293065',
    fontWeight: 'bold',
    fontSize: 20
  },
  activeItem: {
    backgroundColor: '#fef58a',
    fontWeight: 'bold',
    fontSize: 20
  },
})