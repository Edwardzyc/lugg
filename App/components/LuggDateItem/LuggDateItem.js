import * as React from "react";
import {
  View,
  StyleSheet,
  Text
} from "react-native";
import { formattedDate, formattedDay } from '../../utilities/date'

export function LuggDateItem(props) {
  let dayText
  if (props.itemIndex === 0) {
    dayText = 'Today'
  } else if (props.itemIndex === 1) {
    dayText = 'Tomorrow'
  } else {
    dayText = formattedDay(props.date)
  }
  return (
    <View style={[s.item, props.currentItem ? s.activeItem : null]}>
        <Text style={[s.itemText, props.currentItem ? s.activeItemText : null]}>
          { dayText }
          {'\n'}
          { formattedDate(props.date) }
        </Text>
      </View>
  )
}

const s = StyleSheet.create({
  activeItem: {
    borderBottomWidth: 3,
    borderBottomColor: '#dbd484',
  },
  itemText: {
    color: 'white',
    textAlign: 'center',
  },
  activeItemText: {
    color: '#dbd484',
  },
  item: {
    color: 'white',
    height: 60,
    justifyContent: 'center',
    alignItems: 'center'
  }
})