
export function thirtyDays() {
  const today = new Date();
  const year = today.getFullYear();
  const month = today.getMonth();
  const date = today.getDate();
  const dates = [ today ]
  for(var i=1; i < 29; i++){
    dates.push(
      new Date(year, month, date + i)
    )
  }
  return dates
}

export function shortMonth(date) {
  const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
  return monthNames[date.getMonth()]
}

export function formattedDay(date) {
  const dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
  return dayNames[date.getDay()]
}

export function formattedDate(date) {
  return `${shortMonth(date)} ${dayOfMonthWithSuffix(date)}`
}

// https://stackoverflow.com/questions/13627308/add-st-nd-rd-and-th-ordinal-suffix-to-a-number
function dayOfMonthWithSuffix(date) {
  let dayOfMonth = date.getDate()
  var j = dayOfMonth % 10,
      k = dayOfMonth % 100;
  if (j == 1 && k != 11) {
      return dayOfMonth + "st";
  }
  if (j == 2 && k != 12) {
      return dayOfMonth + "nd";
  }
  if (j == 3 && k != 13) {
      return dayOfMonth + "rd";
  }
  return dayOfMonth + "th";
}